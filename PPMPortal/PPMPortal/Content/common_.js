﻿
//common functions ------------------------------------------------------------------------------------------------------
function notifyToast(status, title, subtitle, body) {

    //status list
    //bg-success
    //bg-info
    //bg-warning
    //bg-danger
    //bg-maroon

    $(document).Toasts('create', {
        class: status,
        title: title,
        subtitle: subtitle,
        autohide: true,
        delay: 4000,
        body: body
    })
}

function resetDatatable(tablename) {
    $('#' + tablename + '').DataTable().clear().destroy();
}

function getDateTime(str, isDateOnly) {

    var strTimestamp = str.slice(6, 19);
    var intTimestamp = parseInt(strTimestamp);
    var date = new Date(intTimestamp);
    var strDate;
    var day = ("0" + date.getDate()).slice(-2);
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    //"2021-04-04" format must be this to view in input type date
    // 2021 - 09 - 02 19: 39: 03
    if (isDateOnly) {
        strDate = date.getFullYear() + "-" + month + "-" + day
    } else {

        strDate = date.getFullYear() + "-" + month + "-" + day + " "
            + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    }
    return strDate;
}

function getCurrentDateTime(isDateOnly) {

    var date = new Date();
    var strDate;
    if (isDateOnly) {

        strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate()
    } else {

        strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + " "
            + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    }
    return strDate;
}

//Date range as a button
$('#daterange-btn').daterangepicker(
    {
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
    },
    function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
)

function addColors(first, second, third, fourth, fifth, dark, white) {
    //highlight
    $(first).css('background-color', dark);
    $(second).css('background-color', white);
    $(third).css('background-color', white);
    $(fourth).css('background-color', white);
    $(fifth).css('background-color', white);
}

function loadALoder() {
    var $this = $(this),
        theme = $this.jqmData("theme") || $.mobile.loader.prototype.options.theme,
        msgText = $this.jqmData("msgtext") || $.mobile.loader.prototype.options.text,
        textVisible = $this.jqmData("textvisible") || $.mobile.loader.prototype.options.textVisible,
        textonly = !!$this.jqmData("textonly");
    html = $this.jqmData("html") || "";
    $.mobile.loading("show", {
        text: msgText,
        textVisible: textVisible,
        theme: theme,
        textonly: textonly,
        html: html
    });
    //for hide
    //$.mobile.loading("hide");
}