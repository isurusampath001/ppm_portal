﻿
function loadAllUsers() {

    $('#tblUserList').DataTable().clear().destroy();

    $(document).ready(function () {
        user_tbl = $('#tblUserList').DataTable(
            {
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "dom": 'Bfrtip',
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "ajax": {
                    "url": "/Manager/GetUserList",
                    "type": "POST",
                    "datatype": "json"
                },
                "columnDefs": [
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }, {

                        "targets": 5,
                        "render": function (data, type, row, meta) {

                            var strTimestamp = data.slice(6, 19);
                            var intTimestamp = parseInt(strTimestamp);
                            var date = new Date(intTimestamp);
                            var strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + "  "
                                + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                            return strDate;
                        }
                    },
                    {
                        "targets": 6,
                        "data": "active",
                        "render": function (data, type, row) {
                            //return '<button class="btn btn-info" id="' + row.id + '" onclick="userViewClick(this)"><i class="fas fa-edit"></i> View</button>';
                            return '<button class="btn btn-info" id="' + row.username + '" onclick="userViewClick(this)"><i class="fas fa-edit"></i> View</button><button class="btn btn-danger" id = "' + row.username + '" onclick = "userDeleteClick(this)" > <i class="fas fa-delete"></i>Delete</button > ';
                        }
                    }
                ],
                "columns": [
                    { "data": "username" },
                    { "data": "user_type" },
                    { "data": "group_" },
                    { "data": "email" },
                    { "data": "created_by" },
                    { "data": "created_time" },
                    { "data": "" }
                ]
            }
        )
    });
}

function loadAllQuestions() {
    resetDatatable("tblQuestionList");

    $(document).ready(function () {
        $('#tblQuestionList').DataTable(
            {
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "dom": 'Bfrtip',
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "ajax": {
                    "url": "/Manager/GetQuestionList",
                    "type": "POST",
                    "datatype": "json"
                },
                "columnDefs": [
                    { "width": "10%", "targets": 0 },
                    { "width": "65%", "targets": 1 },
                    { "width": "15%", "targets": 2 },
                    { "width": "10%", "targets": 3 },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    },
                    {

                        "targets": 2,
                        "render": function (data, type, row, meta) {

                            var strTimestamp = data.slice(6, 19);
                            var intTimestamp = parseInt(strTimestamp);
                            var date = new Date(intTimestamp);
                            var strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + "  "
                                + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                            return strDate;
                        }
                    },
                ],
                "columns": [
                    { "data": "question_id" },
                    { "data": "question" },
                    { "data": "created_time" },
                    { "data": "created_by" }
                ]
            }
        )
    });
}

function loadAllScorecards() {
    $(document).ready(function () {

        $('#tblScorecardList').DataTable(
            {
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "dom": 'Bfrtip',
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "ajax": {
                    "url": "/Manager/GetScorecardList",
                    "type": "POST",
                    "datatype": "json"
                },
                "columnDefs": [
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    },
                    {
                        "targets": 2,
                        "render": function (data, type, row, meta) {

                            var strTimestamp = data.slice(6, 19);
                            var intTimestamp = parseInt(strTimestamp);
                            var date = new Date(intTimestamp);
                            var strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + "  "
                                + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                            return strDate;
                        }
                    },
                    {
                        "targets": 3,
                        "render": function (data, type, row, meta) {

                            var strTimestamp = data.slice(6, 19);
                            var intTimestamp = parseInt(strTimestamp);
                            var date = new Date(intTimestamp);
                            var strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + "  "
                                + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                            return strDate;
                        }
                    },
                    {
                        "targets": 4,
                        "render": function (data, type, row, meta) {

                            var strTimestamp = data.slice(6, 19);
                            var intTimestamp = parseInt(strTimestamp);
                            var date = new Date(intTimestamp);
                            var strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + "  "
                                + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                            return strDate;
                        }
                    },
                    {
                        "targets": 6,
                        "data": "active",
                        "render": function (data, type, row) {
                            return '<button class="btn btn-info" type="button" id="' + row.id + '" onclick="scorecardViewClick(this)" ><i class="ion ion-ios-eye"></i> View</button>';
                            
                        }
                    }
                ],
                "columns": [
                    { "data": "score_card_id" },
                    { "data": "name" },
                    { "data": "start_date" },
                    { "data": "to_date" },
                    { "data": "created_time" },
                    { "data": "created_by_name" },
                    { "data": "" }
                ]
            }
        )
    });
}

function loadQuestionsInScorecard(scorecard_id) {

    //reset
    $('#tblViewQuestionsList').DataTable().clear().destroy();

    //iniitiate
    $(document).ready(function () {
        $('#tblViewQuestionsList').DataTable(
            {
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "dom": 'Bfrtip',
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "ajax": {
                    "url": "/Manager/GetQuestionsInScorecard",
                    "type": "POST",
                    "data" : { "scorecard_id": scorecard_id },
                    "datatype": "json"
                },
                "columnDefs": [
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "columns": [
                    { "data": "ques_id" },
                    { "data": "max_score" },
                    { "data": "question" },
                    { "data": "breakdown" }
                ]
            }
        )
    });
}
