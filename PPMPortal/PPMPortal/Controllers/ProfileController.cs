﻿using PPMPortal.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Web.Helpers;
using System;
using System.Globalization;

namespace PPMPortal.Controllers
{
    public class ProfileController : Controller
    {
        public MySqlConnection getConnection()
        {
            string maincon = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
            MySqlConnection mysql = new MySqlConnection(maincon);
            return mysql;
        }

        public ActionResult Index()
        {
            return View();
            //must populate
            //accessable groups,privileges - administration/evaluation/view
            //on view
        }

        public void getConnIPCC()
        {
            //<add name="dbconnectionipcc" connectionString="Data Source=172.25.40.140;port=3306;Initial Catalog=IPCC;User Id=ppmuser;password=slt123@;SslMode=none" />
            string connStr1 = "server=172.25.40.140;user=ppmuser;database=IPCC;password=slt123@;";
            string connStr2 = "server=172.25.40.140:3306;user=ppmuser;database=IPCC;password=slt123@;";

            MySqlConnection conn = new MySqlConnection(connStr1);
            conn.Open();
            string query = "SELECT DISTINCT subOption FROM IPCC.CALL_LOG";
            MySqlCommand comm = new MySqlCommand(query, conn);

            using (MySqlDataReader rdr = comm.ExecuteReader())
            {
                while (rdr.Read())
                {
                    List<string> obj = new List<string>();
                    string s = rdr["subOption"].ToString();
                    obj.Add(s);
                    Console.WriteLine(s);
                }
            }
        }

        public void getConnIPCC2()
        {
            MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();
            conn_string.Server = "172.25.40.140";
            conn_string.UserID = "ppmuser";
            conn_string.Password = "slt123@";
            conn_string.Database = "IPCC";

            MySqlConnection MyCon = new MySqlConnection(conn_string.ToString());

            try
            {
                MyCon.Open();
                Console.WriteLine("hello");
                MyCon.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error : "+ex.Message);
            }
        }

        public JsonResult GetMarksList()
        {
            string username = Session["username"].ToString();

            List<View_Marks> list = new List<View_Marks>();
            MySqlConnection mysql = getConnection();
            string query = "select md_evaluation.score, md_score_card.name, cd_voice_clips.vc_id, cd_voice_clips.taken_date " +
                "from md_evaluation " +
                "join mapping_score_card_question on " +
                "md_evaluation.question_mapping_id = mapping_score_card_question.question_mapping_id " +
                "join md_score_card on " +
                "mapping_score_card_question.score_card_id = md_score_card.score_card_id " +
                "join cd_voice_clips on " +
                "md_evaluation.recording_id = cd_voice_clips.vc_id " +
                "where cd_voice_clips.username = '" + username + "'";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_Marks ev = new View_Marks();

                ev.vc_id = (int)dr["vc_id"];
                ev.name = dr["name"].ToString();
                ev.score = (int)dr["score"];
                ev.taken_date = (DateTime)dr["taken_date"];

                list.Add(ev);
            }
            mysql.Close();

            string strserialize = JsonConvert.SerializeObject(list);
            JsonResult j = Json(new { data = list }, JsonRequestBehavior.AllowGet);

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            //return strserialize;
        }

    }
}