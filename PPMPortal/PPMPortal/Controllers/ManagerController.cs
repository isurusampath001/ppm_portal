﻿using PPMPortal.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;
using System;

namespace PPMPortal.Controllers
{
    public class ManagerController : Controller
    {

        public MySqlConnection getConnection()
        {
            string maincon = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
            MySqlConnection mysql = new MySqlConnection(maincon);
            return mysql;
        }

        public ActionResult IndexHome()
        {
            if (Session["username"] == null || Session["username"].Equals(""))
            {
                return RedirectToAction("IndexLogin", "Login");
            }

            List<SelectListItem> usertypes = new List<SelectListItem>();
            List<SelectListItem> allUsergroups = new List<SelectListItem>();
            List<SelectListItem> usergroups = new List<SelectListItem>();
            List<SelectListItem> languages = new List<SelectListItem>();
            List<SelectListItem> questionTypes = new List<SelectListItem>();
            List<SelectListItem> questions = new List<SelectListItem>();
            List<SelectListItem> scorecardTypes = new List<SelectListItem>();


            foreach (var row in GetUserTypeList())
            {
                usertypes.Add(new SelectListItem()
                {
                    Text = row.user_type.ToString(),
                    Value = row.user_type_id.ToString()
                });
            }

            foreach (var row in GetAllUserGroupList())
            {
                allUsergroups.Add(new SelectListItem()
                {
                    Text = row.group_name.ToString(),
                    Value = row.group_id.ToString()
                });
            }

            foreach (var row in GetUserGroupList())
            {
                usergroups.Add(new SelectListItem()
                {
                    Text = row.group_name.ToString(),
                    Value = row.group_id.ToString()
                });
            }

            foreach (var row in GetLanguageList())
            {
                languages.Add(new SelectListItem()
                {
                    Text = row.language.ToString(),
                    Value = row.IPCC_language_id.ToString()
                });
            }


            foreach (var row in GetQuestionsTypeList())
            {
                questionTypes.Add(new SelectListItem()
                {
                    Text = row.status.ToString(),
                    Value = row.i_id.ToString()
                });
            }

            foreach (var row in GetQuestionsList())
            {
                questions.Add(new SelectListItem()
                {
                    Text = row.question.ToString(),
                    Value = row.question_id.ToString()
                });
            }

            foreach (var row in GetScorecardsTypeList())
            {
                scorecardTypes.Add(new SelectListItem()
                {
                    Text = row.status.ToString(),
                    Value = row.i_id.ToString()
                });
            }

            ViewData["UserTypes"] = new SelectList(usertypes, "Value", "Text");
            ViewData["AllUserGroups"] = new SelectList(allUsergroups, "Value", "Text");
            ViewData["UserGroups"] = new SelectList(usergroups, "Value", "Text");
            ViewData["Languages"] = new SelectList(languages, "Value", "Text");

            ViewData["QuestionTypes"] = new SelectList(questionTypes, "Value", "Text");
            ViewData["Questions"] = new SelectList(questions, "Value", "Text");
            ViewData["ScorecardTypes"] = new SelectList(scorecardTypes, "Value", "Text");
            return View();
        }


        public JsonResult GetUserList()
        {
            string username = Session["username"].ToString();

            List<View_System_User> list = new List<View_System_User>();
            MySqlConnection mysql = getConnection();
            // show result only for user privileged groups
            string query = "select " +
                "su.user_id as user_id," +
                "su.username as username," +
                "su.name as name," +
                "su.username as username," +
                "cut.user_type as user_type," +
                "cug.group_name as group_," +
                "su.email as email," +
                "su.contact_no as contact_no," +
                "su.created_by as created_by," +
                "su.created_by_name as created_by_name," +
                "su.created_time as created_time," +
                "su.ended_by as ended_by," +
                "su.ended_by_name as ended_by_name," +
                "su.ended_time as ended_time " +

                "from system_user as su " +
                "inner join cd_user_group as cug on su.group_id = cug.group_id " +
                "inner join cd_user_type as cut on su.user_type_id = cut.user_type_id " +
                "where su.group_id IN( " +
                "select cug.group_id from cd_user_group as cug " +
                "inner join system_privilege as sp on cug.group_id = sp.privilege_data_id " +
                "WHERE sp.privilege_configuration_id = 4 and sp.username = '" + username + "')";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_System_User u = new View_System_User();

                u.user_id = (int)dr["user_id"];
                u.username = dr["username"].ToString();
                u.name = dr["name"].ToString();
                u.user_type = dr["user_type"].ToString();
                u.group_ = dr["group_"].ToString();
                u.email = dr["email"].ToString();
                u.contact_no = (int)dr["contact_no"];
                u.created_time = (DateTime)dr["created_time"];
                u.created_by = dr["created_by"].ToString();
                u.created_by_name = dr["created_by_name"].ToString();
                u.ended_by = dr["ended_by"].ToString();
                u.ended_by_name = dr["ended_by_name"].ToString();
                u.ended_time = null;// (DateTime)dr["ended_time"];

                list.Add(u);
            }
            mysql.Close();

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            //return strserialize;
        }


        public List<Cd_User_Type> GetUserTypeList()
        {
            List<Cd_User_Type> list = new List<Cd_User_Type>();
            MySqlConnection mysql = getConnection();
            string query = "select * from cd_user_type";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_User_Type ut = new Cd_User_Type();
                ut.user_type_id = (int)dr["user_type_id"];
                ut.user_type = dr["user_type"].ToString();
                ut.created_time = (DateTime)dr["created_time"];
                ut.end_time = null;// (DateTime)dr["created_by"];
                list.Add(ut);
            }
            mysql.Close();
            return list;
        }


        public List<Cd_User_Group> GetAllUserGroupList()
        {
            List<Cd_User_Group> list = new List<Cd_User_Group>();
            MySqlConnection mysql = getConnection();
            string query = "select * from cd_user_group";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_User_Group ug = new Cd_User_Group();
                ug.group_id = (int)dr["group_id"];
                ug.group_name = dr["group_name"].ToString();
                ug.group_code = dr["group_code"].ToString();
                ug.created_time = (DateTime)dr["created_time"];
                ug.end_time = null;// (DateTime)dr["created_by"];
                list.Add(ug);
            }
            mysql.Close();
            return list;
        }

        public List<Cd_User_Group> GetUserGroupList()
        {
            string username = Session["username"].ToString();

            List<Cd_User_Group> list = new List<Cd_User_Group>();
            MySqlConnection mysql = getConnection();

            // show result only for user privileged groups
            string query =
                "select cug.group_id as group_id ,cug.group_name as group_name,cug.group_code as group_code from cd_user_group as cug " +
                "inner join system_privilege as sp on cug.group_id = sp.privilege_data_id " +
                "WHERE sp.privilege_configuration_id = 4 and sp.username = '" + username + "'";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_User_Group ug = new Cd_User_Group();
                ug.group_id = (int)dr["group_id"];
                ug.group_name = dr["group_name"].ToString();
                ug.group_code = dr["group_code"].ToString();
                //ug.created_time = (DateTime)dr["created_time"];
                ug.end_time = null;// (DateTime)dr["created_by"];
                list.Add(ug);
            }
            mysql.Close();
            return list;
        }


        public List<Cd_Language> GetLanguageList()
        {
            string username = Session["username"].ToString();

            List<Cd_Language> list = new List<Cd_Language>();
            MySqlConnection mysql = getConnection();

            // show result only for user privileged groups
            string query =
                "SELECT DISTINCT " +
                "ms.language_id as language_id," +
                "cl.language as language," +
                "cl.IPCC_language_id as IPCC_language_id " +
                "FROM `system_privilege` as sp " +
                "inner join skill_group_privilege as sgp on sp.privilege_data_id = sgp.skill_main_id " +
                "inner join md_skill as ms on sgp.skill_main_id = ms.skill_main_id " +
                "inner join cd_language as cl on ms.language_id = cl.language_id WHERE sp.username = '" + username + "'";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Language ul = new Cd_Language();
                ul.language_id = (int)dr["language_id"];
                ul.language = dr["language"].ToString();
                ul.IPCC_language_id = dr["IPCC_language_id"].ToString();
                list.Add(ul);
            }
            mysql.Close();
            return list;
        }


        public List<Cd_Question> GetQuestionsList()
        {
            List<Cd_Question> list = new List<Cd_Question>();
            MySqlConnection mysql = getConnection();
            string query = "select * from cd_question";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Question qs = new Cd_Question();
                qs.question_id = (int)dr["question_id"];
                qs.question_type = (int)dr["question_type"];
                qs.question = dr["question"].ToString();
                qs.created_time = (DateTime)dr["created_time"];
                qs.ended_time = null;// (DateTime)dr["created_by"];
                list.Add(qs);
            }
            mysql.Close();
            return list;
        }

        public List<Cd_Status> GetQuestionsTypeList()
        {
            List<Cd_Status> list = new List<Cd_Status>();
            MySqlConnection mysql = getConnection();
            string query = "select * from cd_status where s_type_id = 6";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Status s = new Cd_Status();
                s.i_id = (int)dr["i_id"];
                s.status = dr["status"].ToString();
                s.s_type_id = (int)dr["s_type_id"];
                list.Add(s);
            }
            mysql.Close();
            return list;
        }

        public List<Cd_Status> GetScorecardsTypeList()
        {
            List<Cd_Status> list = new List<Cd_Status>();
            MySqlConnection mysql = getConnection();
            string query = "select * from cd_status where s_type_id = 7";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Status s = new Cd_Status();
                s.i_id = (int)dr["i_id"];
                s.status = dr["status"].ToString();
                s.s_type_id = (int)dr["s_type_id"];
                list.Add(s);
            }
            mysql.Close();
            return list;
        }

        public ActionResult getAgents()
        {
            string username = Session["username"].ToString();

            List<System_User> list = new List<System_User>();
            MySqlConnection mysql = getConnection();
            string query =
                "select * from system_user as su " +
                "where su.user_type_id = 4 AND su.group_id IN ( " +
                    "select cug.group_id from cd_user_group as cug inner join system_privilege as sp " +
                    "on cug.group_id = sp.privilege_data_id " +
                    "WHERE sp.privilege_configuration_id = 4 and sp.username = '" + username + "')";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                System_User s = new System_User();
                s.username = dr["username"].ToString();
                s.name = dr["name"].ToString();
                list.Add(s);
            }
            mysql.Close();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult GetUserInfo(string user_name)
        {
            System_User su = new System_User();
            MySqlConnection mysql = getConnection();
            string query = "SELECT * FROM system_user WHERE username = '" + user_name + "' AND is_publish = 1;";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                su.user_id = (int)dr["user_id"];
                su.username = dr["username"].ToString();
                su.name = dr["name"].ToString();
                su.contact_no = (int)dr["contact_no"];
                su.email = dr["email"].ToString();
                su.user_type_id = (int)dr["user_type_id"];
                su.group_id = (int)dr["group_id"];
                su.is_publish = (int)dr["is_publish"];

                su.created_by_name = dr["created_by_name"].ToString();
                su.created_time = (DateTime)dr["created_time"];

            }
            mysql.Close();
            return Json(su, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteUser(string user_name)
        {
            var ended_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
            var endedBy = Session["username"];
            var endedByName = Session["name"];

            System_User su = new System_User();
            MySqlConnection mysql = getConnection();
            string query = "UPDATE system_user SET " +
                "is_publish = 0 AND " +
                "ended_time = '" + ended_time +
                "',ended_by = '" + endedBy +
                "',ended_by_name = '" + endedByName +
                "' WHERE username = '" + user_name + "';";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            comm.ExecuteNonQuery();
            mysql.Close();
            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }


        [HttpPost]
        public JsonResult getUserTypeValues(int id)
        {

            int filterid = 0;
            if (id == 10)
            {
                //manager,evaluator
                filterid = 1;
            }
            else
            {
                filterid = 2;
            }
            List<Cd_User_Type> list = new List<Cd_User_Type>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT * FROM cd_user_type WHERE type_filter = " + filterid + ";";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_User_Type ut = new Cd_User_Type();
                ut.user_type_id = (int)dr["user_type_id"];
                ut.user_type = dr["user_type"].ToString();
                list.Add(ut);
            }
            mysql.Close();

            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult CreateUser(System_User user)
        {
            Session["new_username"] = user.username;

            var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
            var createdBy = Session["username"];
            var createdByName = Session["name"];

            string query = "Insert into system_user" +
                "(username,name,user_type_id,group_id,email,contact_no,is_publish," +
                "created_time,created_by,created_by_name,ended_time,ended_by,ended_by_name) values ('" +
                    user.username + "','" +
                    user.name + "'," +
                    user.user_type_id + "," +
                    user.group_id + ",'" +
                    user.email + "'," +
                    user.contact_no + ",1,'" +
                    created_time + "','" +
                    createdBy + "','" +
                    createdByName + "',NULL,'','')";

            MySqlConnection mysql = getConnection();
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            comm.ExecuteNonQuery();
            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }


        [HttpPost]
        public ActionResult UpdateUser(System_User user)
        {
            Session["updating_username"] = user.username;

            string query = "UPDATE system_user SET " +
                " name = '" + user.name + "'," +
                " email = '" + user.email + "'," +
                " contact_no = '" + user.contact_no + "' " +
                " WHERE username = '" + user.username + "'";

            MySqlConnection mysql = getConnection();
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            comm.ExecuteNonQuery();
            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }


        [HttpPost]
        public ActionResult CreatePrivilege(List<Dictionary<string, string>> arraySysPCon)
        {

            var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
            var new_userName = Session["new_username"];
            var createdBy = Session["username"];
            var createdByName = Session["name"];

            string msg = "";

            for (int x = 0; x < arraySysPCon.Count; x++)
            {
                string query = "Insert into system_privilege" +
                  "(privilege_configuration_id," +
                  "username," +
                  "privilege_data_id," +
                  "created_by," +
                  "created_by_name," +
                  "created_time," +
                  "ended_by," +
                  "ended_by_name," +
                  "ended_time" +
                  ") values (" +
                  "'" + arraySysPCon[x]["KEY_PRIVILEGE_CONFIGURATION_ID"] + "'," +
                  "'" + new_userName + "'," +
                  "'" + arraySysPCon[x]["KEY_SKILL_PRIVILEGE_ID"] + "'," +
                  "'" + createdBy + "'," +
                  "'" + createdByName + "'," +
                  "'" + created_time + "'," +
                  "''," +
                  "''," +
                  "'')";

                MySqlConnection mysql = getConnection();
                MySqlCommand comm = new MySqlCommand(query);
                comm.Connection = mysql;
                mysql.Open();
                comm.ExecuteNonQuery();
                msg = "Success";
            }
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }


        [HttpPost]
        public ActionResult UpdatePrivilege(List<Dictionary<string, string>> arraySysPCon)
        {

            var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
            var updating_username = Session["updating_username"];
            var createdBy = Session["username"];
            var createdByName = Session["name"];

            string msg = "";
            //must change--> assumed every new privileges are new and they will be inserted
            //but correctly same pri,pri loss might be there

            //remove
            string query1 = "delete from system_privilege where username = '" + updating_username + "' ";

            MySqlConnection mysql1 = getConnection();
            MySqlCommand comm1 = new MySqlCommand(query1);
            comm1.Connection = mysql1;
            mysql1.Open();
            comm1.ExecuteNonQuery();
            msg = "Success";


            //add
            for (int x = 0; x < arraySysPCon.Count; x++)
            {
                string query = "Insert into system_privilege " +
                  "(privilege_configuration_id," +
                  "username," +
                  "privilege_data_id," +
                  "created_by," +
                  "created_by_name," +
                  "created_time," +
                  "ended_by," +
                  "ended_by_name," +
                  "ended_time" +
                  ") values (" +
                  "'" + arraySysPCon[x]["KEY_PRIVILEGE_CONFIGURATION_ID"] + "'," +
                  "'" + updating_username + "'," +
                  "'" + arraySysPCon[x]["KEY_SKILL_PRIVILEGE_ID"] + "'," +
                  "'" + createdBy + "'," +
                  "'" + createdByName + "'," +
                  "'" + created_time + "'," +
                  "''," +
                  "''," +
                  "'')";

                MySqlConnection mysql = getConnection();
                MySqlCommand comm = new MySqlCommand(query);
                comm.Connection = mysql;
                mysql.Open();
                comm.ExecuteNonQuery();
                msg = "Success";
            }
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public JsonResult GetPrivilegeListUM(string username)
        {
            List<int> list = new List<int>();
            int group_id;

            string query1 = "SELECT privilege_data_id AS group_id FROM system_privilege " +
                                "WHERE username = '" + username + "' AND privilege_configuration_id = 4 ";

            MySqlConnection mysql1 = getConnection();
            MySqlCommand comm1 = new MySqlCommand(query1);
            comm1.Connection = mysql1;
            mysql1.Open();

            MySqlDataReader dr1 = comm1.ExecuteReader();
            while (dr1.Read())
            {
                group_id = (int)dr1["group_id"];
                list.Add(group_id);
            }
            mysql1.Close();

            List<View_Privilege> privilegeList = new List<View_Privilege>();

            string query2 = "SELECT " +

            "0 AS id," +
            "UG.group_id AS group_id, " +
            "UG.group_code AS group_code, " +
            "UG.group_name AS group_name, " +

            "SPT.privilege_type_id AS privilege_type_id," +
            "SPT.privilege AS privilege_type," +

            "0 AS skill_id," +
            "0 AS skill," +

            "0 AS language_id," +
            "0 AS language," +
            "0 AS IPCC_language_id," +

            "SPC.privilege_configuration_id AS privilege_configuration_id," +
            "SPC.authorized_role AS authorized_role," +

            "0 AS group_privilege_mapping_id," +
            "0 AS skill_main_id, " +
            "0 AS is_active " +

            "FROM " +
            "mapping_group_privilege AS MGP " +
            "JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id " +
            "JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id " +
            "JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id " +
            "WHERE MGP.privilege_configuration_id = 4 ";


            MySqlConnection mysql2 = getConnection();
            MySqlCommand comm2 = new MySqlCommand(query2);
            comm2.Connection = mysql2;
            mysql2.Open();
            MySqlDataReader dr = comm2.ExecuteReader();
            while (dr.Read())
            {
                View_Privilege s = new View_Privilege();

                s.group_id = (int)dr["group_id"];
                s.group_code = dr["group_code"].ToString();
                s.group_name = dr["group_name"].ToString();

                s.language_id = (int)dr["language_id"];
                s.language = dr["language"].ToString();
                s.IPCC_language_id = dr["IPCC_language_id"].ToString();

                s.skill_id = (int)dr["skill_id"];
                s.skill = dr["skill"].ToString();

                s.privilege_type_id = (int)dr["privilege_type_id"];
                s.privilege_type = dr["privilege_type"].ToString();

                s.privilege_configuration_id = (int)dr["privilege_configuration_id"];
                s.authorized_role = dr["authorized_role"].ToString();

                s.skill_main_id = (int)dr["skill_main_id"];
                //s.IPCC_queue_id = (int)dr["IPCC_queue_id"];

                s.group_privilege_mapping_id = (int)dr["group_privilege_mapping_id"];
                s.id = (int)dr["id"];

                for (int i = 0; i < list.Count; i++)
                {
                    if ((int)dr["group_id"] == list[i])
                    {
                        s.is_active = 1;
                    }
                }
                privilegeList.Add(s);
            }
            mysql2.Close();
            return Json(new { data = privilegeList }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetPrivilegeListPM(string username)
        {
            List<Dictionary<string, int>> arr = new List<Dictionary<string, int>>();

            string query1 = "SELECT privilege_configuration_id, privilege_data_id FROM system_privilege " +
                "WHERE username = '" + username + "'  AND " +
                "(privilege_configuration_id = 1 OR privilege_configuration_id = 2 OR privilege_configuration_id = 3)";

            MySqlConnection mysql1 = getConnection();
            MySqlCommand comm1 = new MySqlCommand(query1);
            comm1.Connection = mysql1;
            mysql1.Open();

            MySqlDataReader dr1 = comm1.ExecuteReader();
            while (dr1.Read())
            {
                Dictionary<string, int> val = new Dictionary<string, int>();
                val.Add("KEY_PRIVILEGE_CONFIGURATION_ID", (int)dr1["privilege_configuration_id"]);
                val.Add("KEY_SKILL_PRIVILEGE_ID", (int)dr1["privilege_data_id"]);
                arr.Add(val);
            }
            mysql1.Close();


            List<View_Privilege> privilegeList = new List<View_Privilege>();

            string query2 = "SELECT " +
                "SGP.skill_privilege_id AS id," +

                "UG.group_id AS group_id, " +
                "UG.group_code AS group_code, " +
                "UG.group_name AS group_name, " +

                "SPT.privilege_type_id AS privilege_type_id," +
                "SPT.privilege AS privilege_type," +

                "CDS.skill_id AS skill_id," +
                "CDS.skill AS skill," +

                "CDL.language_id AS language_id," +
                "CDL.language AS language," +
                "CDL.IPCC_language_id AS IPCC_language_id," +

                "SPC.privilege_configuration_id AS privilege_configuration_id," +
                "SPC.authorized_role AS authorized_role," +

                "MGP.group_privilege_mapping_id AS group_privilege_mapping_id," +
                "MDS.skill_main_id AS skill_main_id, " +
                "0 AS is_active " +

                "FROM " +
                "mapping_group_privilege AS MGP " +
                "JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id " +
                "JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id " +
                "JOIN skill_group_privilege AS SGP ON MGP.group_id = SGP.group_id " +

                "JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id " +
                "JOIN md_skill AS MDS ON SGP.skill_main_id = MDS.skill_main_id " +
                "JOIN cd_skill AS CDS ON MDS.skill_id = CDS.skill_id " +
                "JOIN cd_language AS CDL ON MDS.language_id = CDL.language_id " +
                "WHERE MGP.privilege_configuration_id = 1 OR " +
                                "MGP.privilege_configuration_id = 2 OR " +
                                "MGP.privilege_configuration_id = 3 ";

            MySqlConnection mysql2 = getConnection();
            MySqlCommand comm2 = new MySqlCommand(query2);
            comm2.Connection = mysql2;
            mysql2.Open();
            MySqlDataReader dr = comm2.ExecuteReader();
            while (dr.Read())
            {
                View_Privilege s = new View_Privilege();

                s.group_id = (int)dr["group_id"];
                s.group_code = dr["group_code"].ToString();
                s.group_name = dr["group_name"].ToString();

                s.language_id = (int)dr["language_id"];
                s.language = dr["language"].ToString();
                s.IPCC_language_id = dr["IPCC_language_id"].ToString();

                s.skill_id = (int)dr["skill_id"];
                s.skill = dr["skill"].ToString();

                s.privilege_type_id = (int)dr["privilege_type_id"];
                s.privilege_type = dr["privilege_type"].ToString();

                s.privilege_configuration_id = (int)dr["privilege_configuration_id"];
                s.authorized_role = dr["authorized_role"].ToString();

                s.skill_main_id = (int)dr["skill_main_id"];
                //s.IPCC_queue_id = (int)dr["IPCC_queue_id"];

                s.group_privilege_mapping_id = (int)dr["group_privilege_mapping_id"];
                s.id = (int)dr["id"];

                for (int i = 0; i < arr.Count; i++)
                {
                    Dictionary<string, int> k = new Dictionary<string, int>();
                    k = arr[i];

                    if (k["KEY_PRIVILEGE_CONFIGURATION_ID"] == (int)dr["privilege_configuration_id"] &&
                            k["KEY_SKILL_PRIVILEGE_ID"] == (int)dr["id"])
                    {
                        s.is_active = 1;
                    }
                }
                privilegeList.Add(s);
            }
            mysql2.Close();
            return Json(new { data = privilegeList }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetQuestionList()
        {
            List<Cd_Question> list = new List<Cd_Question>();
            MySqlConnection mysql = getConnection();
            string query = "select * from cd_question";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Question q = new Cd_Question();

                q.question_id = (int)dr["question_id"];
                q.question = dr["question"].ToString();
                q.created_time = (DateTime)dr["created_time"];
                q.created_by = dr["created_by"].ToString();
                q.ended_time = null;// (DateTime)dr["ended_time"];
                q.ended_by = dr["ended_by"].ToString();

                list.Add(q);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            //return strserialize;
        }

        public JsonResult GetEvaluatedInfo()
        {
            List<Md_Evaluation> list = new List<Md_Evaluation>();
            MySqlConnection mysql = getConnection();
            string query = "select * from md_evaluation";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Md_Evaluation me = new Md_Evaluation();

                me.evaluation_id = dr["evaluation_id"].ToString();
                me.main_option_id = (int)dr["main_option_id"];
                me.status = (int)dr["status"];
                me.unique_id = dr["unique_id"].ToString();
                me.evaluated_agent = dr["evaluated_agent"].ToString();
                me.skill = dr["skill"].ToString();
                me.lang_id = dr["lang_id"].ToString();
                me.remarks = dr["remarks"].ToString();
                me.created_by_name = dr["created_by_name"].ToString();
                list.Add(me);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);         
        }


        [HttpPost]
        public ActionResult GetQuestions(string scorecardType)
        {
            List<Cd_Question> list = new List<Cd_Question>();
            MySqlConnection mysql = getConnection();
            int ques_type = 0;
            if (scorecardType != null && scorecardType == "24")
            {
                ques_type = 20;
            }
            else
            {
                ques_type = 19;
            }

            string query =
                "select * from cd_question as Q join cd_status as S on " +
                "Q.question_type = S.i_id where Q.question_type = " + ques_type;
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Question gq = new Cd_Question();

                gq.question_id = (int)dr["question_id"];
                gq.question_type = (int)dr["question_type"];
                gq.question = dr["question"].ToString();
                //gq.max_score = (int)dr["max_score"];
                list.Add(gq);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetScorecardList()
        {
            List<Md_Score_Card> list = new List<Md_Score_Card>();
            MySqlConnection mysql = getConnection();
            string query = "select * from md_score_card";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Md_Score_Card sc = new Md_Score_Card();

                sc.score_card_id = (int)dr["score_card_id"];
                sc.name = dr["name"].ToString();
                sc.start_date = (DateTime)dr["start_date"];
                sc.to_date = (DateTime)dr["to_date"];
                sc.created_time = (DateTime)dr["created_time"];
                sc.created_by = dr["created_by"].ToString();
                sc.created_by_name = dr["created_by_name"].ToString();

                list.Add(sc);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetScorecardInfo(string sc_id)
        {
            Md_Score_Card sc = new Md_Score_Card();
            MySqlConnection mysql = getConnection();
            string query = "SELECT * FROM md_score_card WHERE score_card_id = '" + sc_id + "';";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                sc.score_card_id = (int)dr["score_card_id"];
                sc.start_date = (DateTime)dr["start_date"];
                sc.to_date = (DateTime)dr["to_date"];
                sc.name = dr["name"].ToString();
                sc.created_by = dr["created_by"].ToString();
                sc.created_by_name = dr["created_by_name"].ToString();
                sc.created_time = (DateTime)dr["created_time"];
            }
            mysql.Close();
            return Json(sc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuestionsInScorecard(int scorecard_id)
        {
            List<View_Scorecard_Detail_View> list = new List<View_Scorecard_Detail_View>();
            MySqlConnection mysql = getConnection();
            string query =
                "select " +
                "cdq.question_id AS ques_id ," +
                "msq.max_score AS max_score ," +
                "cdq.question AS question ," +
                "msq.score_breakdown AS breakdown " +
                "from cd_question as cdq join mapping_score_card_question as msq on cdq.question_id = msq.question_id " +
                "where msq.score_card_id = " + scorecard_id + "";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_Scorecard_Detail_View obj = new View_Scorecard_Detail_View();
                obj.ques_id = (int)dr["ques_id"];
                obj.max_score = (int)dr["max_score"];
                obj.question = dr["question"].ToString();
                obj.breakdown = dr["breakdown"].ToString();
                list.Add(obj);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateQues(Cd_Question qs)
        {
            var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var ended_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var createdBy = Session["username"];
            var createdByName = Session["name"];

            string query = "Insert into cd_question" +
                "(question,question_type,created_time,created_by,created_by_name,end_time,ended_by,ended_by_name) values ('" +
                    qs.question + "'," +
                    qs.question_type + ",'" +
                    created_time + "','" +
                    createdBy + "','" +
                    createdByName + "','" +
                    ended_time + "','" +
                    qs.ended_by + "','" +
                    qs.ended_by_name + "')";

            MySqlConnection mysql = getConnection();
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            comm.ExecuteNonQuery();
            string msg = "Success";
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult CreateScorecard(Md_Score_Card scorecard)
        {
            var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
            var createdBy = Session["username"];
            var createdByName = Session["name"];

            var start_date = Convert.ToDateTime(scorecard.start_date).ToString("yyyy-MM-dd HH:mm:ss");
            var to_date = Convert.ToDateTime(scorecard.to_date).ToString("yyyy-MM-dd HH:mm:ss");

            string query = "Insert into md_score_card" +
            "(name,sc_type_id,start_date,to_date,created_time,created_by,created_by_name,ended_time,ended_by,ended_by_name) values ('" +
                scorecard.name + "'," +
                scorecard.sc_type_id + ",'" +
                start_date + "','" +
                to_date + "','" +
                created_time + "','" +
                createdBy + "','" +
                createdByName + "','" +
                "','" +
                "','" +
                "')";

            MySqlConnection mysql = getConnection();
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            comm.ExecuteNonQuery();

            Session["lastinsertrowid_forSC"] = comm.LastInsertedId;
            Session["lastinsertrowid_forBK"] = comm.LastInsertedId;
            return Json(new { message = "success", JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult AddQuestionsToScorecard(List<View_Scorecard_Breakdown> questionsArray)
        {
            var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
            var createdBy = Session["username"];
            var createdByName = Session["name"];
            var score_card_id = Session["lastinsertrowid_forSC"];

            string msg = "";
            for (int x = 0; x < questionsArray.Count; x++)
            {
                string breakdown = "";
                try
                {
                    breakdown = string.Join(".", questionsArray[x].breakdown.ToArray());
                }
                catch (Exception)
                {
                    breakdown = "";
                }

                string query = "Insert into mapping_score_card_question " +
                  "(max_score," +
                  "score_card_id," +
                  "question_id," +
                  "score_breakdown," +
                  "created_by," +
                  "created_by_name," +
                  "created_time," +
                  "ended_by," +
                  "ended_by_name," +
                  "ended_time" +
                  ") values (" +
                  questionsArray[x].max_score + "," +
                  score_card_id + "," +
                  questionsArray[x].ques_id + ",'" +
                  breakdown + "'," +
                  "'" + createdBy + "'," +
                  "'" + createdByName + "'," +
                  "'" + created_time + "'," +
                  "''," +
                  "''," +
                  "'')";

                MySqlConnection mysql = getConnection();
                MySqlCommand comm = new MySqlCommand(query);
                comm.Connection = mysql;
                mysql.Open();
                comm.ExecuteNonQuery();
                msg = "Success";
                //should destroy Session["lastinsertrowid_forSC"] value
            }
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        //----------------------------------------------------------
        [HttpPost]
        public JsonResult GetPrivileges(string privilegeType)
        {
            List<View_Privilege> list = new List<View_Privilege>();
            MySqlConnection mysql = getConnection();

            string query = "";

            string query_pm =
                "SELECT " +
                "SGP.skill_privilege_id AS id," +

                "UG.group_id AS group_id, " +
                "UG.group_code AS group_code, " +
                "UG.group_name AS group_name, " +

                "SPT.privilege_type_id AS privilege_type_id," +
                "SPT.privilege AS privilege_type," +

                "CDS.skill_id AS skill_id," +
                "CDS.skill AS skill," +

                "CDL.language_id AS language_id," +
                "CDL.language AS language," +
                "CDL.IPCC_language_id AS IPCC_language_id," +

                "SPC.privilege_configuration_id AS privilege_configuration_id," +
                "SPC.authorized_role AS authorized_role," +

                "MGP.group_privilege_mapping_id AS group_privilege_mapping_id," +
                "MDS.skill_main_id AS skill_main_id, " +
                "0 AS is_active " +

                "FROM " +
                "mapping_group_privilege AS MGP " +
                "JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id " +
                "JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id " +
                "JOIN skill_group_privilege AS SGP ON MGP.group_id = SGP.group_id " +

                "JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id " +
                "JOIN md_skill AS MDS ON SGP.skill_main_id = MDS.skill_main_id " +
                "JOIN cd_skill AS CDS ON MDS.skill_id = CDS.skill_id " +
                "JOIN cd_language AS CDL ON MDS.language_id = CDL.language_id " +
                "WHERE MGP.privilege_configuration_id = 2 OR  MGP.privilege_configuration_id = 3";

            string query_where_admin = " OR MGP.privilege_configuration_id = 1 ";

            string query_um =

                "SELECT " +

                "0 AS id," +

                "UG.group_id AS group_id, " +
                "UG.group_code AS group_code, " +
                "UG.group_name AS group_name, " +

                "SPT.privilege_type_id AS privilege_type_id," +
                "SPT.privilege AS privilege_type," +

                "0 AS skill_id," +
                "0 AS skill," +

                "0 AS language_id," +
                "0 AS language," +
                "0 AS IPCC_language_id," +

                "SPC.privilege_configuration_id AS privilege_configuration_id," +
                "SPC.authorized_role AS authorized_role," +

                "0 AS group_privilege_mapping_id," +
                "0 AS skill_main_id, " +
                "0 AS is_active " +

                "FROM " +
                "mapping_group_privilege AS MGP " +
                "JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id " +
                "JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id " +
                "JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id " +
                "WHERE MGP.privilege_configuration_id = 4 ";

            if (privilegeType == "1")
            {
                query = query_pm + query_where_admin;
            }
            if (privilegeType == "2" || privilegeType == "3")
            {
                query = query_pm;
            }
            else if (privilegeType == "4")
            {
                query = query_um;
            }

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_Privilege s = new View_Privilege();

                s.group_id = (int)dr["group_id"];
                s.group_code = dr["group_code"].ToString();
                s.group_name = dr["group_name"].ToString();

                s.language_id = (int)dr["language_id"];
                s.language = dr["language"].ToString();
                s.IPCC_language_id = dr["IPCC_language_id"].ToString();

                s.skill_id = (int)dr["skill_id"];
                s.skill = dr["skill"].ToString();

                s.privilege_type_id = (int)dr["privilege_type_id"];
                s.privilege_type = dr["privilege_type"].ToString();

                s.privilege_configuration_id = (int)dr["privilege_configuration_id"];
                s.authorized_role = dr["authorized_role"].ToString();

                s.skill_main_id = (int)dr["skill_main_id"];
                //s.IPCC_queue_id = (int)dr["IPCC_queue_id"];

                s.group_privilege_mapping_id = (int)dr["group_privilege_mapping_id"];
                s.id = (int)dr["id"];
                s.is_active = (int)dr["is_active"];


                list.Add(s);
            }
            mysql.Close();

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPrivileges1(string privilegeType)
        {
            List<View_Privilege> list = new List<View_Privilege>();
            MySqlConnection mysql = getConnection();

            string query = "";

            string query_pm =
                "SELECT " +
                "SGP.skill_privilege_id AS id," +

                "UG.group_id AS group_id, " +
                "UG.group_code AS group_code, " +
                "UG.group_name AS group_name, " +

                "SPT.privilege_type_id AS privilege_type_id," +
                "SPT.privilege AS privilege_type," +

                "CDS.skill_id AS skill_id," +
                "CDS.skill AS skill," +

                "CDL.language_id AS language_id," +
                "CDL.language AS language," +
                "CDL.IPCC_language_id AS IPCC_language_id," +

                "SPC.privilege_configuration_id AS privilege_configuration_id," +
                "SPC.authorized_role AS authorized_role," +

                "MGP.group_privilege_mapping_id AS group_privilege_mapping_id," +
                "MDS.skill_main_id AS skill_main_id, " +
                "0 AS is_active " +

                "FROM " +
                "mapping_group_privilege AS MGP " +
                "JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id " +
                "JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id " +
                "JOIN skill_group_privilege AS SGP ON MGP.group_id = SGP.group_id " +

                "JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id " +
                "JOIN md_skill AS MDS ON SGP.skill_main_id = MDS.skill_main_id " +
                "JOIN cd_skill AS CDS ON MDS.skill_id = CDS.skill_id " +
                "JOIN cd_language AS CDL ON MDS.language_id = CDL.language_id " +
                "WHERE MGP.privilege_configuration_id = 2 OR  MGP.privilege_configuration_id = 3";

            string query_where_admin = " OR MGP.privilege_configuration_id = 1 ";

            string query_um =

                "SELECT " +

                "0 AS id," +

                "UG.group_id AS group_id, " +
                "UG.group_code AS group_code, " +
                "UG.group_name AS group_name, " +

                "SPT.privilege_type_id AS privilege_type_id," +
                "SPT.privilege AS privilege_type," +

                "0 AS skill_id," +
                "0 AS skill," +

                "0 AS language_id," +
                "0 AS language," +
                "0 AS IPCC_language_id," +

                "SPC.privilege_configuration_id AS privilege_configuration_id," +
                "SPC.authorized_role AS authorized_role," +

                "0 AS group_privilege_mapping_id," +
                "0 AS skill_main_id, " +
                "0 AS is_active " +

                "FROM " +
                "mapping_group_privilege AS MGP " +
                "JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id " +
                "JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id " +
                "JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id " +
                "WHERE MGP.privilege_configuration_id = 4 ";

            if (privilegeType == "1")
            {
                query = query_pm + query_where_admin;
            }
            if (privilegeType == "2" || privilegeType == "3")
            {
                query = query_pm;
            }
            else if (privilegeType == "4")
            {
                query = query_um;
            }

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_Privilege s = new View_Privilege();

                s.group_id = (int)dr["group_id"];
                s.group_code = dr["group_code"].ToString();
                s.group_name = dr["group_name"].ToString();

                s.language_id = (int)dr["language_id"];
                s.language = dr["language"].ToString();
                s.IPCC_language_id = dr["IPCC_language_id"].ToString();

                s.skill_id = (int)dr["skill_id"];
                s.skill = dr["skill"].ToString();

                s.privilege_type_id = (int)dr["privilege_type_id"];
                s.privilege_type = dr["privilege_type"].ToString();

                s.privilege_configuration_id = (int)dr["privilege_configuration_id"];
                s.authorized_role = dr["authorized_role"].ToString();

                s.skill_main_id = (int)dr["skill_main_id"];
                //s.IPCC_queue_id = (int)dr["IPCC_queue_id"];

                s.group_privilege_mapping_id = (int)dr["group_privilege_mapping_id"];
                s.id = (int)dr["id"];
                s.is_active = (int)dr["is_active"];


                list.Add(s);
            }
            mysql.Close();

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

    }
}