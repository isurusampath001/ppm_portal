﻿using PPMPortal.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Web.Helpers;
using System;
using System.Globalization;

namespace PPMPortal.Controllers
{
    public class TeamLeaderController : Controller
    {
        public MySqlConnection getConnection()
        {
            string maincon = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
            MySqlConnection mysql = new MySqlConnection(maincon);
            return mysql;
        }
        
        //team leader home
        public ActionResult IndexHome()
        {
            if (Session["username"] == null || Session["username"].Equals(""))
            {
                return RedirectToAction("IndexLogin", "Login");
            }
            List<SelectListItem> scorecards = new List<SelectListItem>();
            List<SelectListItem> groups = new List<SelectListItem>();
            List<SelectListItem> skills = new List<SelectListItem>();
            List<SelectListItem> languages = new List<SelectListItem>();
            List<SelectListItem> agents = new List<SelectListItem>();

            foreach (var row in GetScorecardNameListTL())
            {
                scorecards.Add(new SelectListItem()
                {
                    Text = row.name.ToString(),
                    Value = row.score_card_id.ToString()
                });
            }

            foreach (var row in GetGroupListTL())
            {
                groups.Add(new SelectListItem()
                {
                    Text = row.group_name.ToString(),
                    Value = row.group_id.ToString()
                });
            }

            foreach (var row in GetSkillsListTL())
            {
                skills.Add(new SelectListItem()
                {
                    Text = row.skill.ToString(),
                    Value = row.skill_id.ToString()
                });
            }

            foreach (var row in GetLanguagesListTL())
            {
                languages.Add(new SelectListItem()
                {
                    Text = row.language.ToString(),
                    Value = row.language_id.ToString()
                });
            }

            foreach (var row in GetAgentsListTL())
            {
                agents.Add(new SelectListItem()
                {
                    Text = row.name.ToString() + ' ' + row.username.ToString(),
                    Value = row.user_id.ToString()
                });
            }

            ViewData["Scorecards"] = new SelectList(scorecards, "Value", "Text");
            ViewData["Groups"] = new SelectList(groups, "Value", "Text");
            ViewData["Skills"] = new SelectList(skills, "Value", "Text");
            ViewData["Languages"] = new SelectList(languages, "Value", "Text");
            ViewData["Agents"] = new SelectList(agents, "Value", "Text");

            return View();
        }
              
        public List<Md_Score_Card> GetScorecardNameListTL()
        {
            List<Md_Score_Card> list = new List<Md_Score_Card>();
            MySqlConnection mysql = getConnection();
            string query = "select * from md_score_card";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Md_Score_Card scn = new Md_Score_Card();

                scn.score_card_id = (int)dr["score_card_id"];
                scn.name = dr["name"].ToString();

                list.Add(scn);
            }
            mysql.Close();

            return list;

        }
       
        public List<Cd_User_Group> GetGroupListTL()
        {
            string username = Session["username"].ToString();

            List<Cd_User_Group> list = new List<Cd_User_Group>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT distinct cd_user_group.group_id, cd_user_group.group_name " +
                "FROM system_privilege " +
                "JOIN skill_group_privilege ON " +
                "system_privilege.privilege_data_id = skill_group_privilege.skill_privilege_id " +
                "JOIN cd_user_group ON " +
                "cd_user_group.group_id = skill_group_privilege.group_id " +
                "WHERE system_privilege.username = '" + username + "'";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_User_Group ug = new Cd_User_Group();
                ug.group_id = (int)dr["group_id"];
                ug.group_name = dr["group_name"].ToString();
                list.Add(ug);
            }
            mysql.Close();
            return list;
        }
       
        public List<Cd_Skill> GetSkillsListTL()
        {
            string username = Session["username"].ToString();

            List<Cd_Skill> list = new List<Cd_Skill>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT DISTINCT cd_skill.skill_id, cd_skill.skill " +
                "FROM system_privilege " +
                "JOIN skill_group_privilege ON " +
                "system_privilege.privilege_data_id = skill_group_privilege.skill_privilege_id " +
                "JOIN md_skill ON " +
                "md_skill.skill_main_id = skill_group_privilege.skill_main_id  " +
                "JOIN cd_skill ON " +
                "cd_skill.skill_id = md_skill.skill_id " +
                "WHERE system_privilege.username = '"+username+"'";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Skill sk = new Cd_Skill();
                sk.skill_id = (int)dr["skill_id"];
                sk.skill = dr["skill"].ToString();
                list.Add(sk);
            }
            mysql.Close();
            return list;
        }
        
        public List<Cd_Language> GetLanguagesListTL()
        {
            string username = Session["username"].ToString();

            List<Cd_Language> list = new List<Cd_Language>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT DISTINCT cd_language.language_id, cd_language.language " +
                "FROM system_privilege " +
                "JOIN skill_group_privilege ON " +
                "system_privilege.privilege_data_id = skill_group_privilege.skill_privilege_id " +
                "JOIN md_skill ON " +
                "md_skill.skill_main_id = skill_group_privilege.skill_main_id  " +
                "JOIN cd_language ON " +
                "cd_language.language_id = md_skill.language_id " +
                "WHERE system_privilege.username = '"+ username +"'";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Language lg = new Cd_Language();
                lg.language_id = (int)dr["language_id"];
                lg.language = dr["language"].ToString();
                list.Add(lg);
            }
            mysql.Close();
            return list;

        }
      
        public List<System_User> GetAgentsListTL()
        {

            List<System_User> list = new List<System_User>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT system_user.user_id, system_user.name,  system_user.username " +
                "FROM system_user " +
                "JOIN cd_user_type ON " +
                "system_user.user_type_id = cd_user_type.user_type_id " +
                "WHERE cd_user_type.user_type_id = 4";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                System_User su = new System_User();
                su.user_id = (int)dr["user_id"];
                su.name = dr["name"].ToString();
                su.username = dr["username"].ToString();
                list.Add(su);
            }
            mysql.Close();
            return list;
        }
        
        public ActionResult GetVoiceClipInfoTL(string vc_id)
        {
            Cd_Voice_Clips vc = new Cd_Voice_Clips();
            MySqlConnection mysql = getConnection();
            string query = "SELECT * FROM cd_voice_clips WHERE vc_id = '" + vc_id + "'";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                vc.vc_id = (int)dr["vc_id"];
                vc.file_path = dr["file_path"].ToString();
                vc.username = dr["username"].ToString();
                vc.clip_duration = dr["clip_duration"].ToString();
                vc.taken_date = (DateTime)dr["taken_date"];
            }
            mysql.Close();
            return Json(vc, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetScorecardQuestionsTL(int score_card_id)
        {
            List<Cd_Question> list = new List<Cd_Question>();
            MySqlConnection mysql = getConnection();
            string query =
                "select * from cd_question as cdq join mapping_score_card_question as msq on cdq.question_id = msq.question_id " +
                "where msq.score_card_id = " + score_card_id + "";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Question ques = new Cd_Question();

                ques.question = dr["question"].ToString();


                list.Add(ques);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

    }
}