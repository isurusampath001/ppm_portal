﻿using System;
using PPMPortal.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace PPMPortal.Controllers
{
    public class LoginController : Controller
    {
        public MySqlConnection getConnection()
        {
            string maincon = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
            MySqlConnection mysql = new MySqlConnection(maincon);
            return mysql;
        }

        public ActionResult IndexLogin()
        {
            Session.Clear();
            return View();
        }

        [HttpPost]
        public ActionResult ADLogin(Models.System_User u)
        {
            if (ModelState.IsValid)
            {
                var str1 = "";
                var str2 = "";
                if (u.username == null || u.password == null)
                {
                    if (u.username == null || u.username == "")
                    {
                        str2 = "Please Enter the Username";
                        TempData["vaidation"] = str1;
                        return RedirectToAction("IndexLogin", "Login");
                    }
                    if (u.password == null || u.password == "")
                    {
                        str2 = "Please Enter the Password";
                        TempData["vaidation"] = str2;
                        return RedirectToAction("IndexLogin", "Login");
                    }
                    return RedirectToAction("IndexLogin", "Login");
                }
                else
                {
                    string response = "";
                    string[] sArray ;
                    string username = "";
                    string email = "";
                    try
                    {
                        WebReference.ADServices ADServices = new WebReference.ADServices();
                        response = ADServices.userAuthenticated(u.username, u.password);
                        //string s = "015751;Malsha Nandasena;isuru@slt.com.lk";

                        sArray = response.Split(';');
                        username = sArray[0];
                        email = sArray[2];
                    }
                    catch (Exception)
                    {
                        TempData["message"] = "error1";
                        return RedirectToAction("IndexLogin", "Login");
                    }

                    var userList = GetUserList(username, email);
                    var controller = "";

                    if (userList != null && userList.Count != 0 && userList[0].user_type_id != 0)
                    {
                        Session["username"] = userList[0].username.ToString();
                        Session["name"] = userList[0].name.ToString();
                        Session["user_type_id"] = userList[0].user_type_id.ToString();
                        Session["user_group_id"] = userList[0].group_id.ToString();

                        switch (userList[0].user_type_id)
                        {
                            case 1:

                                Session["user_type"] = "Manager";
                                controller = "Manager";
                                break;

                            case 2:

                                Session["user_type"] = "Evaluator";
                                controller = "Evaluator";
                                break;

                            case 3:

                                Session["user_type"] = "Team Leader";
                                controller = "TeamLeader";
                                break;

                            case 4:

                                Session["user_type"] = "Agent";
                                controller = "Agent";
                                break;

                            default:

                                Session["user_type"] = "Agent";
                                controller = "Agent";
                                break;
                        }
                        return RedirectToAction("IndexHome", controller);
                    }
                    else
                    {
                        TempData["vaidation"] = "Invalid Login";
                        return RedirectToAction("IndexLogin", "Login");
                    }
                }
            }
            return null;
        }

        public List<System_User> GetUserList(string username,string email)
        {
            List<System_User> list = new List<System_User>();
            MySqlConnection mysql = getConnection();
            string query = "select * from system_user where username ='"+username+"' and email ='"+email+"'";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            try
            {
                mysql.Open();
                MySqlDataReader dr = comm.ExecuteReader();
                while (dr.Read())
                {
                    System_User u = new System_User();
                    u.user_id = (int)dr["user_id"];
                    u.username = dr["username"].ToString();
                    u.name = dr["name"].ToString();
                    u.user_type_id = (int)dr["user_type_id"];
                    u.group_id = (int)dr["group_id"];
                    u.email = dr["email"].ToString();
                    u.contact_no = (int)dr["contact_no"];
                    u.created_time = null;//(DateTime)dr["created_time"]; //must be correct
                    u.created_by = dr["created_by"].ToString();
                    u.created_by_name = dr["created_by_name"].ToString();
                    u.ended_by = dr["ended_by"].ToString();
                    u.ended_by_name = dr["ended_by_name"].ToString();
                    u.ended_time = null;// (DateTime)dr["ended_time"]; //must be correct

                    list.Add(u);
                }
                mysql.Close();
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult DoLogout()
        {
            Session.Clear();
            ViewData.Clear();
            return RedirectToAction("IndexLogin", "Login");
        }
    }
}