﻿using PPMPortal.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Web.Helpers;
using System;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace PPMPortal.Controllers
{
    public class EvaluatorController : Controller
    {
        public MySqlConnection getConnection()
        {
            string maincon = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
            MySqlConnection mysql = new MySqlConnection(maincon);
            return mysql;
        }

        public MySqlConnection getConnectionipcc()
        {
            string maincon = ConfigurationManager.ConnectionStrings["dbconnectionipcc"].ConnectionString;
            MySqlConnection mysql = new MySqlConnection(maincon);
            return mysql;
        }

        //evaluator home
        public ActionResult IndexHome()
        {
            if (Session["username"] == null || Session["username"].Equals(""))
            {
                return RedirectToAction("IndexLogin", "Login");
            }
            List<SelectListItem> groups = new List<SelectListItem>();
            List<SelectListItem> scorecardsRegular = new List<SelectListItem>();
            List<SelectListItem> scorecardsCallSurvey = new List<SelectListItem>();
            List<SelectListItem> mainoptions = new List<SelectListItem>();
            List<SelectListItem> languages = new List<SelectListItem>();
            List<SelectListItem> agents = new List<SelectListItem>();

            foreach (var row in GetGroupList())
            {
                groups.Add(new SelectListItem()
                {
                    Text = row.group_name.ToString(),
                    Value = row.group_id.ToString()
                });
            }

            foreach (var row in GetRegularScorecardNameList())
            {
                scorecardsRegular.Add(new SelectListItem()
                {
                    Text = row.name.ToString(),
                    Value = row.score_card_id.ToString()
                });
            }

            foreach (var row in GetCallSurveyScorecardNameList())
            {
                scorecardsCallSurvey.Add(new SelectListItem()
                {
                    Text = row.name.ToString(),
                    Value = row.score_card_id.ToString()
                });
            }

            foreach (var row in GetMainOptionList())
            {
                mainoptions.Add(new SelectListItem()
                {
                    Text = row.option_name.ToString(),
                    Value = row.i_id.ToString()
                });
            }

            foreach (var row in GetLanguagesList())
            {
                languages.Add(new SelectListItem()
                {
                    Text = row.language.ToString(),
                    Value = row.IPCC_language_id.ToString()
                });
            }

            foreach (var row in GetAgentsList())
            {
                agents.Add(new SelectListItem()
                {
                    Text = row.name.ToString() + ' ' + row.username.ToString(),
                    Value = row.username.ToString()
                });
            }

            ViewData["Groups"] = new SelectList(groups, "Value", "Text");
            ViewData["RegularSC"] = new SelectList(scorecardsRegular, "Value", "Text");
            ViewData["CallSurveySC"] = new SelectList(scorecardsCallSurvey, "Value", "Text");
            ViewData["MainOptions"] = new SelectList(mainoptions, "Value", "Text");
            ViewData["Languages"] = new SelectList(languages, "Value", "Text");
            ViewData["Agents"] = new SelectList(agents, "Value", "Text");

            return View();
        }

        public List<Md_Score_Card> GetRegularScorecardNameList()
        {
            List<Md_Score_Card> list = new List<Md_Score_Card>();
            MySqlConnection mysql = getConnection();
            string query = "select * from md_score_card where sc_type_id != 24";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Md_Score_Card scn = new Md_Score_Card();
                scn.score_card_id = (int)dr["score_card_id"];
                scn.name = dr["name"].ToString();
                list.Add(scn);
            }
            mysql.Close();
            return list;
        }

        public List<Md_Score_Card> GetCallSurveyScorecardNameList()
        {
            List<Md_Score_Card> list = new List<Md_Score_Card>();
            MySqlConnection mysql = getConnection();
            string query = "select * from md_score_card where sc_type_id = 24";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Md_Score_Card scn = new Md_Score_Card();
                scn.score_card_id = (int)dr["score_card_id"];
                scn.name = dr["name"].ToString();
                list.Add(scn);
            }
            mysql.Close();
            return list;
        }

        public List<Cd_User_Group> GetGroupList()
        {
            string username = Session["username"].ToString();
            List<Cd_User_Group> list = new List<Cd_User_Group>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT distinct cd_user_group.group_id, cd_user_group.group_name " +
                "FROM system_privilege " +
                "JOIN skill_group_privilege ON " +
                "system_privilege.privilege_data_id = skill_group_privilege.skill_privilege_id " +
                "JOIN cd_user_group ON " +
                "cd_user_group.group_id = skill_group_privilege.group_id " +
                "WHERE system_privilege.username = '" + username + "'";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_User_Group ug = new Cd_User_Group();
                ug.group_id = (int)dr["group_id"];
                ug.group_name = dr["group_name"].ToString();
                list.Add(ug);
            }
            mysql.Close();
            return list;
        }

        public List<Cd_Main_Option> GetMainOptionList()
        {
            List<Cd_Main_Option> list = new List<Cd_Main_Option>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT * FROM cd_main_option";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Main_Option mo = new Cd_Main_Option();
                mo.i_id = (int)dr["i_id"];
                mo.option_name = dr["option_name"].ToString();
                list.Add(mo);
            }
            mysql.Close();
            return list;
        }

        [HttpPost]
        public JsonResult getSkillList(int mainOptionid)
        {
            string username = Session["username"].ToString();
            List<Cd_Skill> list = new List<Cd_Skill>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT DISTINCT cd_skill.skill_id, cd_skill.skill " +
                "FROM system_privilege " +
                "JOIN skill_group_privilege ON " +
                "system_privilege.privilege_data_id = skill_group_privilege.skill_privilege_id " +
                "JOIN md_skill ON " +
                "md_skill.skill_main_id = skill_group_privilege.skill_main_id  " +
                "JOIN cd_skill ON " +
                "cd_skill.skill_id = md_skill.skill_id " +
                "WHERE system_privilege.username = '" + username + "' AND cd_skill.main_option = " + mainOptionid;

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Skill sk = new Cd_Skill();
                sk.skill_id = (int)dr["skill_id"];
                sk.skill = dr["skill"].ToString();
                list.Add(sk);
            }
            mysql.Close();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public List<Cd_Language> GetLanguagesList()
        {
            string username = Session["username"].ToString();
            List<Cd_Language> list = new List<Cd_Language>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT DISTINCT cd_language.language_id, cd_language.language,cd_language.IPCC_language_id " +
                "FROM system_privilege " +
                "JOIN skill_group_privilege ON " +
                "system_privilege.privilege_data_id = skill_group_privilege.skill_privilege_id " +
                "JOIN md_skill ON " +
                "md_skill.skill_main_id = skill_group_privilege.skill_main_id  " +
                "JOIN cd_language ON " +
                "cd_language.language_id = md_skill.language_id " +
                "WHERE system_privilege.username = '" + username + "'";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                Cd_Language lg = new Cd_Language();
                lg.language_id = (int)dr["language_id"];
                lg.language = dr["language"].ToString();
                lg.IPCC_language_id = dr["IPCC_language_id"].ToString();
                list.Add(lg);
            }
            mysql.Close();
            return list;
        }

        public List<System_User> GetAgentsList()
        {
            List<System_User> list = new List<System_User>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT system_user.user_id, system_user.name,  system_user.username " +
                "FROM system_user " +
                "JOIN cd_user_type ON " +
                "system_user.user_type_id = cd_user_type.user_type_id " +
                "JOIN cd_user_group ON " +
                "system_user.group_id = cd_user_group.group_id " +
                "WHERE cd_user_type.user_type_id = 4";
            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                System_User su = new System_User();
                su.user_id = (int)dr["user_id"];
                su.name = dr["name"].ToString();
                su.username = dr["username"].ToString();
                list.Add(su);
            }
            mysql.Close();
            return list;
        }

        [HttpPost]
        public JsonResult GetSelectedVoiceClip(View_Selected_Voice_Clip voiceClip)
        {
            MySqlConnection mysql = getConnectionipcc();

            string query = "SELECT " +
                "UNIQUEID, " +
                "talk_time " +
                "FROM IPCC.CALL_LOG C where " +
                "CALL_TIME >= '" + voiceClip.from_date + "' AND " +
                "CALL_TIME <= '" + voiceClip.to_date + "' AND " +
                "mainOption='" + voiceClip.main_option + "' AND " +
                "subOption='" + voiceClip.skill + "' AND " +
                "lang ='" + voiceClip.language_id + "' AND " +
                "enteredQueue='YES' AND " +
                "isAnswered='YES' AND " +
                "agent='" + voiceClip.agent + "' " +
                "ORDER BY RAND() LIMIT 1";

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            try
            {
                mysql.Open();
                MySqlDataReader dr = comm.ExecuteReader();
                while (dr.Read())
                {
                    View_Voice_Clip_Main vc = new View_Voice_Clip_Main();

                    vc.UNIQUEID = dr["UNIQUEID"].ToString();
                    vc.talk_time = Convert.ToInt32(dr["talk_time"]);

                    Session["UniqID"] = dr["UNIQUEID"].ToString();

                    //calling api
                    string url = "http://172.25.40.129:60001/";
                    string uid = "?uid=" + vc.UNIQUEID;
                    //string uid = "?uid=1632553939.10040";

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(url);
                        //HTTP GET
                        var responseTask = client.GetAsync("api/Voiceext1" + uid);
                        responseTask.Wait();
                        var result = responseTask.Result;

                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<IList<string>>();
                            readTask.Wait();
                            var alldata = readTask.Result;

                            List<string> fval = new List<string>();
                            foreach (string x in alldata)
                            {
                                if (x != "")
                                {
                                    fval.Add(x);
                                }
                            }
                            return Json(fval);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                mysql.Close();
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        [HttpPost]
        public JsonResult GetSelectedVoiceClipTest(View_Selected_Voice_Clip voiceClip)
        {
            List<View_Voice_Clip_Main> list = new List<View_Voice_Clip_Main>();
            MySqlConnection mysql = getConnectionipcc();

            string queryTest = "SELECT " +
                "UNIQUEID, " +
                "talk_time " +
                "FROM IPCC.CALL_LOG C where " +
                "date(CALL_TIME) between '2021-09-01' and '2021-09-30' and " +
                "mainOption='4' and " +
                "subOption='extragb' and " +
                "enteredQueue='YES' and " +
                "isAnswered='YES' and " +
                "agent='007199' " +
                "ORDER BY RAND() LIMIT 1";

            MySqlCommand comm = new MySqlCommand(queryTest);
            comm.Connection = mysql;
            try
            {
                mysql.Open();
                MySqlDataReader dr = comm.ExecuteReader();
                while (dr.Read())
                {
                    View_Voice_Clip_Main vc = new View_Voice_Clip_Main();

                    vc.UNIQUEID = dr["UNIQUEID"].ToString();
                    vc.talk_time = Convert.ToInt32(dr["talk_time"]);
                    Session["UniqID"] = dr["UNIQUEID"].ToString();

                    //calling api
                    string url = "http://172.25.40.129:60001/";
                    string uid = "?uid=" + vc.UNIQUEID;
                    //string uid = "?uid=1632553939.10040"; //two clips load

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(url);
                        //HTTP GET
                        var responseTask = client.GetAsync("api/Voiceext1" + uid);
                        responseTask.Wait();
                        var result = responseTask.Result;

                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<IList<string>>();
                            readTask.Wait();
                            var alldata = readTask.Result;

                            List<string> fval = new List<string>();
                            foreach (string x in alldata)
                            {
                                if (x != "")
                                {
                                    fval.Add(x);
                                }
                            }
                            return Json(fval);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                mysql.Close();
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        [HttpPost]
        public JsonResult EvaluateVoiceClip(Md_Evaluation eval)
        {
            string msg = "";
            if (eval != null)
            {
                string query;
                var uniqueId = Session["UniqID"];
                var created_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); //2008-10-03 22:59:52
                var createdBy = Session["username"];
                var createdByName = Session["name"];

                //username+timestamp (values not gonna duplicate)
                var evaluationId = createdBy + "" + eval.evaluation_id;

                query = "Insert into md_evaluation (" +
                        "evaluation_id," +
                        "unique_id," +
                        "lead_id,recording_id," +
                        "evaluated_agent," +
                        "main_option_id," +
                        "skill," +
                        "lang_id," +
                        "status," +
                        "remarks," +
                        "created_time,created_by,created_by_name,ended_time,ended_by,ended_by_name) values ('" +
                            evaluationId + "','" +
                            uniqueId + "'," +
                            "'','','" +
                            eval.evaluated_agent + "'," +
                            eval.main_option_id + ",'" +
                            eval.skill + "','" +
                            eval.lang_id + "',1,'" +
                            eval.remarks + "','" +
                            created_time + "','" +
                            createdBy + "','" +
                            createdByName + "','" +
                            "','" +
                            "','" +
                            "')";

                MySqlConnection mysql = getConnection();
                MySqlCommand comm = new MySqlCommand(query);
                comm.Connection = mysql;
                mysql.Open();
                comm.ExecuteNonQuery();
                msg = "success";
            }
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult EvaluateVoiceClipResult(List<Md_Evaluated_Question> evalQues)
        {
            var createdBy = Session["username"];
            string msg = "", query;
            if (evalQues != null && evalQues.Count > 0)
            {
                int listSize = evalQues.Count;

                for (int i = 0; i < listSize; i++)
                {
                    string evaluationId = createdBy + "" +evalQues[i].evaluation_id;
                    query = "Insert into md_evaluated_question (" +
                            "evaluation_id," +
                            "ques_mapping_id," +
                            "score) values ('" +
                                evaluationId + "'," +
                                evalQues[i].ques_mapping_id + ",'" +
                                evalQues[i].score + "')";

                    MySqlConnection mysql = getConnection();
                    MySqlCommand comm = new MySqlCommand(query);
                    comm.Connection = mysql;
                    mysql.Open();
                    comm.ExecuteNonQuery();
                }

                msg = "Success";
            }
            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }

        public JsonResult GetScorecardQuestions(int score_card_id)
        {
            List<View_Scorecard_Detail_View> list = new List<View_Scorecard_Detail_View>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT " +
                "MSCD.question_mapping_id AS question_mapping_id," +
                "CQ.question_id AS question_id," +
                "MSCD.max_score AS max_score," +
                "CQ.question AS question," +
                "MSCD.score_breakdown AS score_breakdown " +
                "FROM `mapping_score_card_question` AS MSCD INNER JOIN `cd_question` AS CQ " +
                "ON MSCD.question_id = CQ.question_id WHERE MSCD.score_card_id = " + score_card_id;

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_Scorecard_Detail_View vsdv = new View_Scorecard_Detail_View();
                vsdv.question_mapping_id = (int)dr["question_mapping_id"];
                vsdv.ques_id = (int)dr["question_id"];
                vsdv.max_score = (int)dr["max_score"];
                vsdv.question = dr["question"].ToString();
                vsdv.breakdown = dr["score_breakdown"].ToString();
                list.Add(vsdv);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCallQualityScorecardQuestions(int score_card_id)
        {
            List<View_Scorecard_Detail_View> list = new List<View_Scorecard_Detail_View>();
            MySqlConnection mysql = getConnection();
            string query = "SELECT " +
                "MSCD.question_mapping_id AS question_mapping_id," +
                "CQ.question_id AS question_id," +
                "CQ.question AS question," +
                "MSCD.score_breakdown AS score_breakdown " +
                "FROM `mapping_score_card_question` AS MSCD INNER JOIN `cd_question` AS CQ " +
                "ON MSCD.question_id = CQ.question_id WHERE MSCD.score_card_id = " + score_card_id;

            MySqlCommand comm = new MySqlCommand(query);
            comm.Connection = mysql;
            mysql.Open();
            MySqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                View_Scorecard_Detail_View vsdv = new View_Scorecard_Detail_View();
                vsdv.question_mapping_id = (int)dr["question_mapping_id"];
                vsdv.ques_id = (int)dr["question_id"];
                vsdv.question = dr["question"].ToString();
                vsdv.breakdown = dr["score_breakdown"].ToString();
                list.Add(vsdv);
            }
            mysql.Close();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
    }
}