﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class View_Privilege
    {
        public int group_id { get; set; }
        public string group_code { get; set; }
        public string group_name { get; set; }

        public int language_id { get; set; }
        public string language { get; set; }
        public string IPCC_language_id { get; set; }

        public int skill_id { get; set; }
        public string skill { get; set; }


        public int privilege_type_id { get; set; }
        public string privilege_type { get; set; }

        public int privilege_configuration_id { get; set; }

        public string authorized_role { get; set; }

        public int group_privilege_mapping_id { get; set; }

        public int skill_main_id { get; set; }
        //public int IPCC_queue_id { get; set; } //temporary

        public int id { get; set; }//skill_privilege_id //data should compare from this table 
        public int is_active { get; set; }


        /*
        SELECT 
SGP.skill_privilege_id AS skill_privilege_id,

UG.group_id AS group_id, 
UG.group_code AS group_code, 
UG.group_name AS group_name, 

SPT.privilege_type_id AS privilege_type_id,
SPT.privilege AS privilege_type,

CDS.skill_id AS skill_id,
CDS.skill AS skill,

CDL.language_id AS language_id,
CDL.language AS language,
CDL.IPCC_language_id AS IPCC_language_id,

SPC.privilege_configuration_id AS privilege_configuration_id,
SPC.authorized_role AS authorized_role,

MGP.group_privilege_mapping_id AS group_privilege_mapping_id,

MDS.skill_main_id AS skill_main_id

FROM 
mapping_group_privilege AS MGP 
JOIN cd_system_privilege_configuration AS SPC ON MGP.privilege_configuration_id = SPC.privilege_configuration_id 
JOIN cd_system_privilege_type AS SPT ON SPC.privilege_type_id = SPT.privilege_type_id 

JOIN skill_group_privilege AS SGP ON MGP.group_id = SGP.group_id 

JOIN cd_user_group AS UG ON MGP.group_id = UG.group_id
JOIN md_skill AS MDS ON SGP.skill_main_id = MDS.skill_main_id 
JOIN cd_skill AS CDS ON MDS.skill_id = CDS.skill_id 
JOIN cd_language AS CDL ON MDS.language_id = CDL.language_id   
         */
    }
}


