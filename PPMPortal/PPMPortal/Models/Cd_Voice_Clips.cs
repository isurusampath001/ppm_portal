﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class Cd_Voice_Clips
    {
        public int vc_id { get; set; }

        public string file_path { get; set; }

        public string username { get; set; }

        public string clip_duration { get; set; }

        public Nullable<System.DateTime> taken_date { get; set; }
    }
}