using System;

namespace PPMPortal.Models
{
    public class Mapping_Group_User_Type
    {
        public int group_user_mapping_id { get; set; }
        public int group_id { get; set; }
        public int user_type_id { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
