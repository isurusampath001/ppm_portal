﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class Cd_System_All
    {
        public Cd_Language language { get; set; }
        public Cd_Question question { get; set; }
        public Cd_Skill skill { get; set; }
        public Cd_System_Privilege_configuration sysPrivilegeConfig { get; set; }
        public Cd_System_Privilege_type sysPrivilegeType { get; set; }
        public Cd_User_Group userGroup { get; set; }
        public Cd_User_Type userType { get; set; }

        public System_Privilege systemPri { get; set; }
        public IEnumerable<System_User> systemUser { get; set; }

    }


}


