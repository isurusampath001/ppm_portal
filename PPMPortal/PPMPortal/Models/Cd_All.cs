﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class Cd_All
    {
        public Cd_Language language { get; set; }
        public Cd_Question question { get; set; }
        public Cd_Main_Option mainOption { get; set; }
        public Cd_Skill skill { get; set; }
        public Cd_System_Privilege_configuration sysPrivilegeConfig { get; set; }
        public Cd_System_Privilege_type sysPrivilegeType { get; set; }
        public Cd_User_Group userGroup { get; set; }
        public Cd_User_Type userType { get; set; }
        public Cd_User_Group groupName { get; set; }
        public Cd_Status status { get; set; }
        public Md_Score_Card scorecardName { get; set; }
    }


}


