using System;

namespace PPMPortal.Models
{
    public class Cd_User_Group
    {
        public int group_id { get; set; }
        public string group_name { get; set; }
        public string group_code { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
