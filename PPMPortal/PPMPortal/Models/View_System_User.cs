using System;

namespace PPMPortal.Models
{
    public class View_System_User
    {
        public int user_id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string user_type { get; set; }
        public string group_ { get; set; }
        public string email { get; set; }
        public int contact_no { get; set; }
        public string created_by { get; set; }
        public string created_by_name { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public string ended_by { get; set; }
        public string ended_by_name { get; set; }
        public Nullable<System.DateTime> ended_time { get; set; }
    }
}
