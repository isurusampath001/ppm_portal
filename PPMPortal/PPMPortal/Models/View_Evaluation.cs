using System;
using System.Collections.Generic;

namespace PPMPortal.Models
{
    public class View_Evaluation
    {
        public int i_id { get; set; }

        public List<View_Evaluated_Question> quesList;
        public string lead_id { get; set; }
        public string recording_id { get; set; }
        public string evaluated_agent { get; set; }
        public int main_option_id { get; set; }
        public string skill { get; set; }
        public string lang_id { get; set; }
        public string remarks { get; set; }
    }
}
