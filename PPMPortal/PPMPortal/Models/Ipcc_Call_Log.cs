﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class Ipcc_Call_Log
    {
        public int i_id { get; set; }
        public string UNIQUE_ID { get; set; }
        public int talk_time { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public int mainOption { get; set; }
        public string subOption { get; set; }
        public string enteredQueue { get; set; }
        public string isAnswered { get; set; }
        public int agent { get; set; }
    }
}