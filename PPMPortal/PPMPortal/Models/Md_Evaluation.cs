using System;

namespace PPMPortal.Models
{
    public class Md_Evaluation
    {
        public int i_id { get; set; }
        public string evaluation_id { get; set; }
        public string unique_id { get; set; }
        public string lead_id { get; set; }
        public string recording_id { get; set; }
        public string evaluated_agent { get; set; }
        public int main_option_id { get; set; }
        public string skill { get; set; }
        public string lang_id { get; set; }
        public int status { get; set; }
        public string remarks { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public string created_by { get; set; }
        public string created_by_name { get; set; }
        public Nullable<System.DateTime> ended_time { get; set; }
        public string ended_by { get; set; }
        public string ended_by_name { get; set; }
    }
}