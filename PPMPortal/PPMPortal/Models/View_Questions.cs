﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class View_Questions
    {
        public int question_id { get; set; }
        public string question { get; set; }
        public int max_score { get; set; }
        public int is_active { get; set; }
    }
}