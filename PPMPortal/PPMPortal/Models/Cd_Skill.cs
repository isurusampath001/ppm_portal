using System;

namespace PPMPortal.Models
{
    public class Cd_Skill
    {
        public int skill_id { get; set; }
        public string skill { get; set; }
        public int main_option { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
