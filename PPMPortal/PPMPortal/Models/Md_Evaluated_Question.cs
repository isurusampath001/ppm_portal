using System;

namespace PPMPortal.Models
{
    public class Md_Evaluated_Question
    {
        public int i_id { get; set; }
        public string evaluation_id { get; set; }
        public int ques_mapping_id { get; set; }
        public string score { get; set; }
    }
}
