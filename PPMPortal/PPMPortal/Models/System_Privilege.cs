using System;

namespace PPMPortal.Models
{
    public class System_Privilege
    {
        public int privilege_id { get; set; }
        public string privilege_configuration_id { get; set; }
        public string username { get; set; }
        public string privilege_data_id { get; set; }
        public string created_by { get; set; }
        public string created_by_name { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public string ended_by { get; set; }
        public string ended_by_name { get; set; }
        public Nullable<System.DateTime> ended_time { get; set; }
    }
}
