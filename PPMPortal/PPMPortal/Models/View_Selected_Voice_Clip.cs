﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class View_Selected_Voice_Clip
    {
        public int vc_id { get; set; }
        public int group_id { get; set; }
        public string group_name { get; set; }
        public int main_option { get; set; }
        public int skill_id { get; set; }
        public string skill { get; set; }
        public int language_id { get; set; }
        public string language { get; set; }
        public string agent { get; set; }
        public string UNIQUEID { get; set; }
        public int talk_time { get; set; }
        public Nullable<System.DateTime> from_date { get; set; }
        public Nullable<System.DateTime> to_date { get; set; }

    }
}