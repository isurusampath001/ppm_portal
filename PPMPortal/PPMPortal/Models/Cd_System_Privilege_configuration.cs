using System;

namespace PPMPortal.Models
{
    public class Cd_System_Privilege_configuration
    {
        public int privilege_configuration_id { get; set; }
        public int privilege_type_id { get; set; }
        public string authorized_role { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
