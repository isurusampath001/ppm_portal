using System;

namespace PPMPortal.Models
{
    public class Cd_Language
    {
        public int language_id { get; set; }
        public string language { get; set; }
        public string IPCC_language_id { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
