﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class View_Scorecard_Detail_View
    {
        public int question_mapping_id { get; set; }

        public int ques_id { get; set; }

        public int max_score { get; set; }

        public string question { get; set; }

        public string breakdown { get; set; }

    }


}


