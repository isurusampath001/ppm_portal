using System;

namespace PPMPortal.Models
{
    public class Md_Score_Card
    {
        public int score_card_id { get; set; }
        public string name { get; set; }
        public int sc_type_id { get; set; }
        public DateTime start_date { get; set; }
        public DateTime to_date { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public string created_by { get; set; }
        public string created_by_name { get; set; }
        public Nullable<System.DateTime> ended_time { get; set; }
        public string ended_by { get; set; }
        public string ended_by_name { get; set; }
    }
}
