using System;

namespace PPMPortal.Models
{
    public class Mapping_Group_Privilege
    {
        public int group_privilege_mapping_id { get; set; }
        public int group_id { get; set; }
        public int privilege_configuration_id { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
