﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPMPortal.Models
{
    public class View_Marks
    {
        public int vc_id { get; set; }

        public string name { get; set; }

        public int score { get; set; }

        public Nullable<System.DateTime> taken_date { get; set; }
    }
}