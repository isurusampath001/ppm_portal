using System;

namespace PPMPortal.Models
{
    public class Mapping_Score_Card_Question
    {
        public int question_mapping_id { get; set; }
        public int max_score { get; set; }
        public int score_card_id { get; set; }
        public int question_id { get; set; }
        public string score_breakdown { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public string created_by { get; set; }
        public string created_by_name { get; set; }
        public Nullable<System.DateTime> ended_time { get; set; }
        public string ended_by { get; set; }
        public string ended_by_name { get; set; }
    }
}
