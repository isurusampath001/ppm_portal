using System;

namespace PPMPortal.Models
{
    public class Skill_Group_Privilege
    {
        public int skill_privilege_id { get; set; }
        public int group_id { get; set; }
        public int skill_main_id { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
