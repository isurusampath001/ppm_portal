using System;

namespace PPMPortal.Models
{
    public class Cd_Question
    {
        public int question_id { get; set; }
        public string question { get; set; }
        public int question_type { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public string created_by { get; set; }
        public string created_by_name { get; set; }
        public Nullable<System.DateTime> ended_time { get; set; }
        public string ended_by { get; set; }
        public string ended_by_name { get; set; }
    }
}
