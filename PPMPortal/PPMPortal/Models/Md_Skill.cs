using System;

namespace PPMPortal.Models
{
    public class Md_Skill
    {
        public int skill_main_id { get; set; }
        public int skill_id { get; set; }
        public int language_id { get; set; }
        public int IPCC_queue_id { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
