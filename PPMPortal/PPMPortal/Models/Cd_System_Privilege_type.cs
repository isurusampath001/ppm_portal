using System;

namespace PPMPortal.Models
{
    public class Cd_System_Privilege_type
    {
        public int privilege_type_id { get; set; }
        public string privilege { get; set; }
        public Nullable<System.DateTime> created_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
    }
}
