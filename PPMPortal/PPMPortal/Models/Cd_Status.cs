using System;

namespace PPMPortal.Models
{
    public class Cd_Status
    {
        public int i_id { get; set; }
        public int s_type_id { get; set; }
        public string status  { get; set; }
    }
}
