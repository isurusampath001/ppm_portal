-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2021 at 05:35 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pm`
--

-- --------------------------------------------------------

--
-- Table structure for table `cd_language`
--

CREATE TABLE `cd_language` (
  `language_id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `IPCC_language_id` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_language`
--

INSERT INTO `cd_language` (`language_id`, `language`, `IPCC_language_id`, `created_time`, `end_time`) VALUES
(1, 'Sinhala', 'LAN223', '2021-07-11 05:23:08', '0000-00-00 00:00:00'),
(2, 'English', 'LAN224', '2021-08-02 05:23:08', '0000-00-00 00:00:00'),
(3, 'Tamil', 'LAN225', '2021-08-10 06:23:08', '0000-00-00 00:00:00'),
(4, 'Chinese', 'LAN226', '2021-08-10 03:23:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cd_question`
--

CREATE TABLE `cd_question` (
  `question_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_by_name` varchar(50) NOT NULL,
  `end_time` datetime NOT NULL,
  `ended_by` varchar(50) NOT NULL,
  `ended_by_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_question`
--

INSERT INTO `cd_question` (`question_id`, `question`, `created_time`, `created_by`, `created_by_name`, `end_time`, `ended_by`, `ended_by_name`) VALUES
(1, 'does the agent greets first', '2021-08-09 11:20:23', '1', 'isuru sampath', '0000-00-00 00:00:00', '', ''),
(2, 'does the agent tone arguable', '2021-08-09 11:25:23', '1', 'isuru sampath', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cd_skill`
--

CREATE TABLE `cd_skill` (
  `skill_id` int(11) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_skill`
--

INSERT INTO `cd_skill` (`skill_id`, `skill`, `created_time`, `end_time`) VALUES
(1, 'Faults', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'New Connection info', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Fibre line info', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cd_system_privilege_configuration`
--

CREATE TABLE `cd_system_privilege_configuration` (
  `privilege_configuration_id` int(11) NOT NULL,
  `privilege_type_id` int(11) NOT NULL,
  `authorized_role` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_system_privilege_configuration`
--

INSERT INTO `cd_system_privilege_configuration` (`privilege_configuration_id`, `privilege_type_id`, `authorized_role`, `created_time`, `end_time`) VALUES
(1, 1, 'team leader', '2021-07-22 09:20:09', '0000-00-00 00:00:00'),
(2, 1, 'evaluator', '2021-07-22 09:25:09', '0000-00-00 00:00:00'),
(3, 2, 'manager', '2021-07-22 09:25:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cd_system_privilege_type`
--

CREATE TABLE `cd_system_privilege_type` (
  `privilege_type_id` int(11) NOT NULL,
  `privilege` varchar(255) NOT NULL,
  `creted_time` datetime NOT NULL,
  `ended_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_system_privilege_type`
--

INSERT INTO `cd_system_privilege_type` (`privilege_type_id`, `privilege`, `creted_time`, `ended_date`) VALUES
(1, 'Performance management', '2021-08-11 15:44:50', '0000-00-00 00:00:00'),
(2, 'Administration', '2021-08-11 15:46:50', '0000-00-00 00:00:00'),
(3, 'Finance ', '2021-08-11 15:47:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cd_user_group`
--

CREATE TABLE `cd_user_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_code` varchar(20) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_user_group`
--

INSERT INTO `cd_user_group` (`group_id`, `group_name`, `group_code`, `created_time`, `end_time`) VALUES
(1, 'Manager group', 'mgr001', '2021-08-10 05:43:22', '0000-00-00 00:00:00'),
(2, 'Kandy group', 'kdy002', '2021-08-10 05:44:22', '0000-00-00 00:00:00'),
(3, 'colombo group 1', 'col011', '2021-08-10 05:45:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cd_user_type`
--

CREATE TABLE `cd_user_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cd_user_type`
--

INSERT INTO `cd_user_type` (`user_type_id`, `user_type`, `created_time`, `end_time`) VALUES
(1, 'Manager', '2021-08-12 05:40:52', '0000-00-00 00:00:00'),
(2, 'Evaluator', '2021-08-12 05:45:12', '0000-00-00 00:00:00'),
(3, 'Team Leader', '2021-08-12 05:47:52', '0000-00-00 00:00:00'),
(4, 'Agent', '2021-08-12 05:55:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mapping_group_privilege`
--

CREATE TABLE `mapping_group_privilege` (
  `group_privilege_mapping_id` int(11) NOT NULL,
  `group_id` int(20) NOT NULL,
  `privilege_configuration_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mapping_group_user_type`
--

CREATE TABLE `mapping_group_user_type` (
  `group_user_mapping_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mapping_score_card_question`
--

CREATE TABLE `mapping_score_card_question` (
  `question_mapping_id` int(11) NOT NULL,
  `question_weight` int(11) NOT NULL,
  `score_card_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `max_score` int(11) NOT NULL,
  `created_titme` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `ended_time` datetime NOT NULL,
  `ended_by` varchar(50) NOT NULL,
  `ended_by_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `md_evaluation`
--

CREATE TABLE `md_evaluation` (
  `i_id` int(11) NOT NULL,
  `evaluation_id` varchar(20) NOT NULL,
  `question_mapping_id` varchar(20) NOT NULL,
  `score` int(11) NOT NULL,
  `unique_id` varchar(20) NOT NULL,
  `lead_id` varchar(20) NOT NULL,
  `recording_id` varchar(20) NOT NULL,
  `evaluated_agent` varchar(20) NOT NULL,
  `skill_main_data_id` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `ended_time` datetime NOT NULL,
  `ended_by` varchar(20) NOT NULL,
  `ended_by_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `md_score_card`
--

CREATE TABLE `md_score_card` (
  `score_card_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `ended_time` datetime NOT NULL,
  `ended_by` varchar(50) NOT NULL,
  `ended_by_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `md_skill`
--

CREATE TABLE `md_skill` (
  `skill_main_id` int(11) NOT NULL,
  `skill_id` int(3) NOT NULL,
  `language_id` int(2) NOT NULL,
  `IPCC_queue_id` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `skill_group_privilege`
--

CREATE TABLE `skill_group_privilege` (
  `skill_privilege_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `skill_main_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `system_privilege`
--

CREATE TABLE `system_privilege` (
  `privilege_id` int(11) NOT NULL,
  `privilege_configuration_id` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `privilege_data_id` varchar(50) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_by_name` varchar(50) NOT NULL,
  `ended_time` datetime NOT NULL,
  `ended_by` varchar(50) NOT NULL,
  `ended_by_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `system_user`
--

CREATE TABLE `system_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` int(10) NOT NULL,
  `created_time` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_by_name` varchar(255) NOT NULL,
  `ended_time` datetime NOT NULL,
  `ended_by` varchar(50) NOT NULL,
  `ended_by_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_user`
--

INSERT INTO `system_user` (`user_id`, `username`, `name`, `user_type_id`, `group_id`, `email`, `contact_no`, `created_time`, `created_by`, `created_by_name`, `ended_time`, `ended_by`, `ended_by_name`) VALUES
(1, 'isuru91', 'Isuru Sampath', 1, 1, 'isurusampath@slt.com.lk', 712232329, '0000-00-00 00:00:00', '12', 'kasun', '0000-00-00 00:00:00', '0', ''),
(2, 'rfrfrf', 'tgtgtgt', 0, 565665, 'vfvfvfvfv', 232323, '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', ''),
(3, 'pubudu93', 'pubudu wijesinha', 0, 1134, 'pubudu112@gmail.com', 712382799, '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', ''),
(4, 'dcdcd', 'rrrrrrrrrrr', 0, 5555555, 'kkkkkkkk', 33333333, '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cd_language`
--
ALTER TABLE `cd_language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `cd_question`
--
ALTER TABLE `cd_question`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `cd_skill`
--
ALTER TABLE `cd_skill`
  ADD PRIMARY KEY (`skill_id`);

--
-- Indexes for table `cd_system_privilege_configuration`
--
ALTER TABLE `cd_system_privilege_configuration`
  ADD PRIMARY KEY (`privilege_configuration_id`);

--
-- Indexes for table `cd_system_privilege_type`
--
ALTER TABLE `cd_system_privilege_type`
  ADD PRIMARY KEY (`privilege_type_id`);

--
-- Indexes for table `cd_user_group`
--
ALTER TABLE `cd_user_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `cd_user_type`
--
ALTER TABLE `cd_user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- Indexes for table `mapping_group_privilege`
--
ALTER TABLE `mapping_group_privilege`
  ADD PRIMARY KEY (`group_privilege_mapping_id`);

--
-- Indexes for table `mapping_group_user_type`
--
ALTER TABLE `mapping_group_user_type`
  ADD PRIMARY KEY (`group_user_mapping_id`);

--
-- Indexes for table `mapping_score_card_question`
--
ALTER TABLE `mapping_score_card_question`
  ADD PRIMARY KEY (`question_mapping_id`);

--
-- Indexes for table `md_score_card`
--
ALTER TABLE `md_score_card`
  ADD PRIMARY KEY (`score_card_id`);

--
-- Indexes for table `md_skill`
--
ALTER TABLE `md_skill`
  ADD PRIMARY KEY (`skill_main_id`);

--
-- Indexes for table `skill_group_privilege`
--
ALTER TABLE `skill_group_privilege`
  ADD PRIMARY KEY (`skill_privilege_id`);

--
-- Indexes for table `system_privilege`
--
ALTER TABLE `system_privilege`
  ADD PRIMARY KEY (`privilege_id`);

--
-- Indexes for table `system_user`
--
ALTER TABLE `system_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cd_language`
--
ALTER TABLE `cd_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cd_question`
--
ALTER TABLE `cd_question`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cd_skill`
--
ALTER TABLE `cd_skill`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cd_system_privilege_configuration`
--
ALTER TABLE `cd_system_privilege_configuration`
  MODIFY `privilege_configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cd_system_privilege_type`
--
ALTER TABLE `cd_system_privilege_type`
  MODIFY `privilege_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cd_user_group`
--
ALTER TABLE `cd_user_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cd_user_type`
--
ALTER TABLE `cd_user_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mapping_group_privilege`
--
ALTER TABLE `mapping_group_privilege`
  MODIFY `group_privilege_mapping_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mapping_group_user_type`
--
ALTER TABLE `mapping_group_user_type`
  MODIFY `group_user_mapping_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mapping_score_card_question`
--
ALTER TABLE `mapping_score_card_question`
  MODIFY `question_mapping_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `md_score_card`
--
ALTER TABLE `md_score_card`
  MODIFY `score_card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `md_skill`
--
ALTER TABLE `md_skill`
  MODIFY `skill_main_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skill_group_privilege`
--
ALTER TABLE `skill_group_privilege`
  MODIFY `skill_privilege_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_privilege`
--
ALTER TABLE `system_privilege`
  MODIFY `privilege_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_user`
--
ALTER TABLE `system_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
